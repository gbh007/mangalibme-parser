package main

import (
	"app/analyzer"
	"app/report"
	"flag"
	"log"
)

func main() {
	userID := flag.Int64("id", 0, "user id")
	flag.Parse()

	if *userID < 1 {
		log.Fatalln("no user id")
	}

	rep, err := analyzer.Analyze(*userID)
	if err != nil {
		log.Fatalln(err)
	}

	err = report.Render(*rep)
	if err != nil {
		log.Fatalln(err)
	}
}
