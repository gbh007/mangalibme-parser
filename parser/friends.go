package parser

import (
	"app/cacher"
	"fmt"
	"regexp"
	"strconv"
	"time"
)

type UserInfo struct {
	ID   int64
	Icon string
	Name string
}

var (
	friendRegExp = regexp.MustCompile(`(?sm)div\s+` +
		regexp.QuoteMeta(`class="friends-item"`) +
		`.+?` +
		regexp.QuoteMeta(`class="friends-item__avatar-wrap avatar_md"`) +
		`\s+` +
		regexp.QuoteMeta(`href="https://mangalib.me/user/`) +
		`(\d+)".+?` + // User ID
		`data-src="(.+?)".+?` + // Icon
		regexp.QuoteMeta(`class="friends-item__name">`) +
		`(.+?)` + // User name
		`</a>`,
	)
	profileRegExp = regexp.MustCompile(`(?sm)` +
		regexp.QuoteMeta(`<div class="profile-user__avatar-wrap">`) +
		`\s+` +
		regexp.QuoteMeta(`<img src="/uploads/users/`) +
		`(\d+)/(.+?)"\s+alt="(.+?)"`, // User ID + raw icon + name
	)
)

func ParseUserFriends(userID int64) ([]UserInfo, error) {
	rawData, err := cacher.Request(fmt.Sprintf("https://mangalib.me/user/%d/following", userID), time.Minute*5)
	if err != nil {
		return nil, err
	}

	result := []UserInfo{}

	// Сам пользователь
	for _, row := range profileRegExp.FindAllStringSubmatch(string(rawData), -1) {
		if len(row) != 4 {
			continue
		}
		ui := UserInfo{
			Icon: row[2],
			Name: row[3],
		}

		ui.ID, _ = strconv.ParseInt(row[1], 10, 64)
		ui.Icon = fmt.Sprintf("https://mangalib.me/uploads/users/%d/%s", ui.ID, ui.Icon)

		result = append(result, ui)
	}

	// Друзья
	for _, row := range friendRegExp.FindAllStringSubmatch(string(rawData), -1) {
		if len(row) != 4 {
			continue
		}
		ui := UserInfo{
			Icon: row[2],
			Name: row[3],
		}

		ui.ID, _ = strconv.ParseInt(row[1], 10, 64)

		result = append(result, ui)
	}

	return result, nil
}
