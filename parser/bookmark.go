package parser

import (
	"app/cacher"
	"encoding/json"
	"fmt"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

type BookmarkInfo struct {
	MangaName string `json:"manga_name,omitempty"`
	RusName   string `json:"rus_name,omitempty"`
	Slug      string `json:"slug,omitempty"`
	MangaID   int64  `json:"manga_id,omitempty"`
	Cover     string `json:"cover,omitempty"`
	SiteID    int64  `json:"site_id,omitempty"`
	Comment   string `json:"comment,omitempty"`
	BookID    int64  `json:"book_id,omitempty"`
	// 1-Читаю 2-В планах 3-Брошено 4-Прочитано 5-Любимые
	Status    int64  `json:"status,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
	UpdatedAt string `json:"updated_at,omitempty"`
	Dsc       string `json:"dsc,omitempty"`

	LastChapterAt string   `json:"last_chapter_at,omitempty"`
	LastChapter   Chapter  `json:"last_chapter,omitempty"`
	LastBookmark  Bookmark `json:"lastBookmark,omitempty"`

	CreatedAtTime     time.Time `json:"-"`
	UpdatedAtTime     time.Time `json:"-"`
	LastChapterAtTime time.Time `json:"-"`
}

type Chapter struct {
	ID       int64  `json:"id,omitempty"`
	Number   string `json:"number,omitempty"`
	Volume   int64  `json:"volume,omitempty"`
	Name     string `json:"name,omitempty"`
	BranchID int64  `json:"branch_id,omitempty"`
	Status   int64  `json:"status,omitempty"`
}

type Bookmark struct {
	ID       int64  `json:"id,omitempty"`
	Number   string `json:"number,omitempty"`
	Volume   int64  `json:"volume,omitempty"`
	Slug     string `json:"slug,omitempty"`
	BranchID int64  `json:"branch_id,omitempty"`
	PageID   int64  `json:"page_id,omitempty"`
}

func (bi *BookmarkInfo) Parse() {
	bi.CreatedAtTime, _ = time.Parse(timeFormat, bi.CreatedAt)
	bi.UpdatedAtTime, _ = time.Parse(timeFormat, bi.UpdatedAt)
	bi.LastChapterAtTime, _ = time.Parse(timeFormat, bi.LastChapterAt)
}

func (bi *BookmarkInfo) IconURL() string {
	return fmt.Sprintf(`https://mangalib.me/uploads/cover/%s/cover/%s_thumb.jpg`, bi.Slug, bi.Cover)
}

func (bi *BookmarkInfo) TitleURL() string {
	return fmt.Sprintf(`https://mangalib.me/%s?section=info`, bi.Slug)
}

func ParseUserBookmarks(userID int64) ([]BookmarkInfo, error) {
	rawData, err := cacher.Request(fmt.Sprintf("https://mangalib.me/bookmark/%d", userID), time.Minute*5)
	if err != nil {
		return nil, err
	}

	data := struct {
		Items []BookmarkInfo
	}{}

	err = json.Unmarshal(rawData, &data)
	if err != nil {
		return nil, err
	}

	return data.Items, nil
}
