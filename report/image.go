package report

import (
	"app/cacher"
	"encoding/base64"
	"net/http"
	"time"
)

type Image struct {
	URL      string
	B64      string
	MimeType string
}

func endpointToImageWrapped(endpoint string) Image {
	img, _ := endpointToImage(endpoint)
	if img != nil {
		return *img
	}

	return Image{}
}

func endpointToImage(endpoint string) (*Image, error) {
	data, err := cacher.Request(endpoint, time.Hour*24*30)
	if err != nil {
		return nil, err
	}

	return &Image{
		URL:      endpoint,
		MimeType: http.DetectContentType(data),
		B64:      base64.StdEncoding.EncodeToString(data),
	}, nil
}
