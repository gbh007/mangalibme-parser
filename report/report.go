package report

import (
	"app/analyzer"
	"html/template"
	"os"
)

func Render(data analyzer.Report) error {
	templ, err := template.New("").Funcs(template.FuncMap{
		"getImage": endpointToImageWrapped,
		"status":   status,
	}).Parse(rawTemplate)
	if err != nil {
		return err
	}

	file, err := os.Create("report.html")
	if err != nil {
		return err
	}
	defer file.Close()

	err = templ.Execute(file, data)
	if err != nil {
		return err
	}

	return nil
}

func status(s int64) string {
	switch s {
	case 1:
		return "Читает"
	case 2:
		return "В планах"
	case 3:
		return "Бросил"
	case 4:
		return "Прочитал"
	case 5:
		return "В любимых"
	default:
		return ""
	}
}
