package report

const rawTemplate = `<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body>
		<style>
			img.title-icon,
			img.user-icon {
				max-width: 100px;
				max-height: 100px;
			}
			td {
				padding: 5px;
				border-bottom: 1px solid lightgrey;
				border-right: 1px solid lightgrey;
			}
			table {
				border-spacing: 0px 0px;
			}
			td:last-child {
				border-right: none;
			}
			tr:last-child td{
				border-bottom: none;
			}
		</style>
		<table>
			<tbody>
				<tr>
					<td></td>
					{{range $.Users}}
						<td>
							{{with getImage .Icon}}
								<img src="data:{{.MimeType}};base64, {{.B64}}" class="user-icon"/>
							{{end}}
							<br/>
							{{.Name}}
						</td>
					{{end}}
				</tr>
				{{range $titleIndex, $title := $.Titles}}
					<tr>
						<td>
							{{with getImage $title.Icon}}
								<img src="data:{{.MimeType}};base64, {{.B64}}" class="title-icon"/>
							{{end}}
							<a href="{{.URL}}">{{.RusName}}</a>
						</td>
						{{range $.Users}}
							<td>
								{{with index .Titles $title.ID}}
									{{status .Status}}
								{{end}}
							</td>
						{{end}}
					</tr>
				{{end}}
			</tbody>
		</table>
	</body>
</html>
`
