package cacher

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"time"
)

const cacheDir = "cache"

var (
	errNoCache               = errors.New("no cache")
	errCacheLiveTimeExpiried = errors.New("cache live time expiried")
)

func nameFromEndpoint(endpoint string) string {
	name := fmt.Sprintf("%x", md5.Sum([]byte(endpoint)))
	return path.Join(cacheDir, name+".cache")
}

func Request(endpoint string, liveTime time.Duration) ([]byte, error) {
	data, err := fromCache(endpoint, liveTime)
	if err == nil {
		return data, nil
	}

	data, err = request(endpoint)
	if err != nil {
		return nil, err
	}

	_ = toCache(endpoint, data)

	return data, nil
}

func fromCache(endpoint string, liveTime time.Duration) ([]byte, error) {
	name := nameFromEndpoint(endpoint)

	stat, err := os.Stat(name)
	if errors.Is(err, os.ErrNotExist) {
		return nil, errNoCache
	}

	if stat.ModTime().Add(liveTime).Before(time.Now()) {
		return nil, errCacheLiveTimeExpiried
	}

	file, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	data, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func toCache(endpoint string, data []byte) error {
	_, err := os.Stat(cacheDir)
	if errors.Is(err, os.ErrNotExist) {
		_ = os.Mkdir(cacheDir, 0777)
	}

	file, err := os.Create(nameFromEndpoint(endpoint))
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func request(endpoint string) ([]byte, error) {
	resp, err := http.DefaultClient.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}
