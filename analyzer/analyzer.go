package analyzer

import (
	"app/parser"
	"sort"
)

func Analyze(userID int64) (*Report, error) {
	report := &Report{}

	users, err := parser.ParseUserFriends(userID)
	if err != nil {
		return nil, err
	}

	allTitles := []parser.BookmarkInfo{}

	for _, user := range users {
		bookmarks, err := parser.ParseUserBookmarks(user.ID)
		if err != nil {
			return nil, err
		}

		allTitles = append(allTitles, bookmarks...)

		userInfo := UserInfo{
			ID:     user.ID,
			Icon:   user.Icon,
			Name:   user.Name,
			Titles: analyzeBookmarks(bookmarks),
		}

		report.Users = append(report.Users, userInfo)
	}

	report.Titles = handleTitles(allTitles)

	return report, nil
}

func handleTitles(input []parser.BookmarkInfo) []TitleInfo {
	tmp := analyzeBookmarks(input)

	result := []TitleInfo{}

	for _, title := range tmp {
		result = append(result, title)
	}

	sort.SliceStable(result, func(i, j int) bool {
		a, b := result[i], result[j]

		if a.RusName < b.RusName {
			return true
		}
		if a.RusName > b.RusName {
			return false
		}

		return a.ID < b.ID
	})

	return result
}

func analyzeBookmarks(input []parser.BookmarkInfo) map[int64]TitleInfo {
	result := map[int64]TitleInfo{}

	for _, bi := range input {
		if _, ok := result[bi.MangaID]; !ok {
			result[bi.MangaID] = TitleInfo{
				ID:             bi.MangaID,
				Name:           bi.MangaName,
				RusName:        bi.RusName,
				Icon:           bi.IconURL(),
				URL:            bi.TitleURL(),
				Status:         bi.Status,
				FirstAdded:     bi.CreatedAtTime,
				CurrentVolume:  bi.LastBookmark.Volume,
				CurrentChapter: bi.LastBookmark.Number,
				LastUpdate:     bi.LastChapterAtTime,
				LastVolume:     bi.LastChapter.Volume,
				LastChapter:    bi.LastChapter.Number,
			}

			continue
		}
		title := result[bi.MangaID]

		// Проверяем время добавления закладки
		if title.FirstAdded.IsZero() ||
			!bi.CreatedAtTime.IsZero() && bi.CreatedAtTime.Before(title.FirstAdded) {
			title.Status = bi.Status
			title.FirstAdded = bi.CreatedAtTime
			title.CurrentVolume = bi.LastBookmark.Volume
			title.CurrentChapter = bi.LastBookmark.Number
		}

		// Проверяем время последней главы
		if title.LastUpdate.IsZero() ||
			!bi.LastChapterAtTime.IsZero() && bi.LastChapterAtTime.After(title.LastUpdate) {
			title.LastUpdate = bi.LastChapterAtTime
			title.LastVolume = bi.LastChapter.Volume
			title.LastChapter = bi.LastChapter.Number
		}

		result[bi.MangaID] = title
	}

	return result
}
