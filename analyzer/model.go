package analyzer

import "time"

type Report struct {
	Users  []UserInfo
	Titles []TitleInfo
}

type UserInfo struct {
	ID     int64
	Icon   string
	Name   string
	Titles map[int64]TitleInfo
}

type TitleInfo struct {
	ID int64

	Name    string
	RusName string
	// Ссылка на иконку тайтла
	Icon string
	// Ссылка на сам тайтл
	URL string

	// 1-Читаю 2-В планах 3-Брошено 4-Прочитано 5-Любимые
	Status int64
	// Впервые добавлена пользователем
	FirstAdded time.Time
	// Текущий том (у пользователя)
	CurrentVolume int64
	// Текущая глава (у пользователя)
	CurrentChapter string

	// Последнее обновление (выход главы)
	LastUpdate time.Time
	// Последний том
	LastVolume int64
	// Последняя глава
	LastChapter string
}
